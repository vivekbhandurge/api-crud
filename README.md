#Project Name
 
 Crud-api
 
 -----------------------------------------------------------------------------------------------------
 
 #Technology used
 
 Backend - Node.js,Express.js,Mongoose ORM
 
 Database - Mongodb
 
 Mobile Application - React Native
 
 ------------------------------------------------------------------------------------------------------
 
 # Installation Guide
 
 To install npm package - npm install or npm i
 
 To run the project - npm run start
 
 -------------------------------------------------------------------------------------------------------
 # versioning tool
 
 git bitbucket for version controll
 
 jira for task tracking and project management
 
 -------------------------------------------------------------------------------------------------------
 
 # project architecture
 
 
 
 
 
 config - maintains commmon configurations including database config and other thrid party configurations
 
 lib - maintains common global functionalities
 
 local\en - contains global common messages
 
 controller - provides http routes to the services
 
 service - maintains database queries to the database
 
 model - maintains database document schema and mongoose functions (pre default)
 

 -----------------------------------------------------------------------------------------------------

 # NATIVE APP

 ### Project Name
 * ListComponent
 
### Technology used
 * react native
 
### Run project
 * To install npm package - yarn
 * For iOS go to iOS path of project and run in terminal - pod install 
 * To run the project on For Android- react-native run-android
 * To run the project on For iOS- react-native run-is
 
### versioning tool
 
 * git bitbucket for version controll
 * jira for task tracking and project management
 
* Package .json - contains all dependencies
* Entry point for project - index.js 
 
 ### project architecture
 * api folder - Api written in Api folder
 * redux folder for actions and reducers
 * routes folder for routing
 * Screen folder for all screens
 * components folder for All components
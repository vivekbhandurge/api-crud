const swaggerAutogen = require('swagger-autogen')()

const doc = {
    info: {
        version: "1.0.0",
        title: "User service server",
        description: "User service"
    },
    host: "localhost:3003",
	basePath: "/",
    schemes: ['http'],
    consumes: ['application/json'],
    produces: ['application/json'],
    tags: [],
    securityDefinitions: {
        api_key: {
            type: "token",
            name: "x-access-token",
            in: "header"
        }
    },
    definitions: {}
};

const outputFile = './swagger_output.json'
                    
const endpointsFiles = [
    './src/controller/user',
    
    
];

console.log("here in swagger")

swaggerAutogen(outputFile, endpointsFiles, doc).then(() => {
    require('./bin/www') // Your project's root file
});

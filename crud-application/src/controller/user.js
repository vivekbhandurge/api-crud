const express = require('express');
const router = express.Router();
const UserService = require("../services/userService");




router.post('/register-user', (req, res, next)=>{
  /*
      #swagger.path = '/users/register-user'
      #swagger.tags = ['User']
      #swagger.description = 'register user with user information'
      #swagger.parameters['User'] = {
              in: 'body',
              required: true,
              description: "register user",
              schema: {
                
                  name: '',
                  email: '',
                  contactNumber : ''
                }
     }
      #swagger.responses[200] ={}
     */
  return UserService.UserRegister(req, res, next);
});



  
  router.post('/allprofiles',function(req, res, next) {
    /*
  
  #swagger.path = '/users/allprofiles'
  #swagger.tags = ['User']
  #swagger.description = 'get all user profile Information with pagination'
    
    #swagger.parameters['User'] = {
                  in: 'body',
                  required: true,
                  description: "get all user information",
                  schema: {
                     pageNo : ' ',
                     recordsPerPage : ' ' 
                    }
         }
  
   #swagger.description = 'get all user profile Information with pagination.'
  */
    /* #swagger.responses[200] = {}
                    */
  return  UserService.GetAllUsers(req, res, next);
  });


  router.post('/update-user-information/:id',(req, res, next)=>{
    /*
        #swagger.path = '/users/update-user-information/{id}'
        #swagger.tags = ['User']
        #swagger.description = 'update registered user information'
  
        
        #swagger.parameters['User'] = {
  
                 
                 
                 
                in: 'body',
                required: false,
                description: "update user information",
                schema: {
                   
                    name: '',
                    email: '',
                   contactNumber:''
                  },
        
                  
       }
  
                     
              
        #swagger.responses[200] ={}
       */
    return UserService.UpdateRegisteredUser(req, res, next);
  });

  router.post('/delete-user-information/:id',(req, res, next)=>{
    /*
        #swagger.path = '/users/delete-user-information/{id}'
        #swagger.tags = ['User']
        #swagger.description = 'update registered user information'
  
        
        #swagger.parameters['User'] = {
  
                 
                 
                 
                in: 'body',
                required: false,
                description: "update user information",
                schema: {
                   
                    is_active:''
                  },
        
                  
       }
  
                     
              
        #swagger.responses[200] ={}
       */
    return UserService.DeleteUser(req, res, next);
  });

  


module.exports = router;
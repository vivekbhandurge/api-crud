const Schema = Mongoose.Schema;
const UserSchema = Mongoose.Schema(
    {
        email: { type: String, unique: true, sparse: true },
        name: { type: String, trim: true },
         contactNumber:{ type: String, unique: true, sparse: true },
        is_active : {type : Boolean , default : true}
    },
    {
        timestamps: true,
        id: false,
        toObject: {
            virtuals: true,
            getters: true
        },
        toJSON: {
            virtuals: true,
            getters: true
        }
    }
);

module.exports = Mongoose.model('User', UserSchema);

global.Mongoose = require('mongoose');
global.ObjectId = Mongoose.Types.ObjectId;

const dbConfig = require('../../config/default.json').Database;
const options = {
    useCreateIndex: true,
    useNewUrlParser: true,
    dbName: dbConfig.dbName,
    poolSize: 25,
    reconnectTries: 60,
    reconnectInterval: 1000,
    bufferMaxEntries: 0,
    useUnifiedTopology: true,
    useFindAndModify: false,
};
Mongoose.connect(dbConfig.URL, options);
const mongodb = Mongoose.connection;
mongodb.on('error', console.error.bind(console, 'connection error:'));
mongodb.once('open', function() {
    console.log('db connection developed.');
});
const User = require("./userModel");


Mongoose.set('debug', dbConfig.Debug);
let database = {User: User};
Mongoose.set('useFindAndModify', false);
database.mongoose = Mongoose;
module.exports = database;

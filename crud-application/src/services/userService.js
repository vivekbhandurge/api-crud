const UserRegister = (req, res, next) => {
    const {
    
      name,
      email,
      contactNumber
    } = req.body;
  
    const user = new Models.User({
   
      name: name,
      email: email,
      contactNumber: contactNumber
    });
  
    Models.User.find({ contactNumber: contactNumber }, (err, cb) => {
      if (cb.length == 0) {
        user.save((err, cb) => {
          if (err) {
            console.log("User save err: ", err);
            return next(setError(MSG.INTERNAL_ERROR, 500));
          } else {
            console.log("callback-->>", cb);
          
            return res.send({
              data: cb,
              message: MSG.REGISTER_SUCCESS,
            });
          }
        });
      } else if (cb.length > 0) {
        console.log("here inside else");
        return res.send({
            data: null,
            message: "User Contact Number Already Exists",
        });
      }
     
    });
  };

  const GetAllUsers = (req, res, next)=>{
    
     let page  = req.body.pageNo;
      let pageNo = parseInt(page);
      let recordsPerPage = req.body.recordsPerPage
     let recordsNo = parseInt(recordsPerPage);
       Models.User.find({is_active:true}).skip(recordsNo*(pageNo-1)).limit(recordsNo).then((userList)=>{
           if(!userList){
               return next(setError(MSG.DATA_NOT_FOUND));
           }else {
               return res.send({data: userList,count:recordsNo, message: MSG.DATA_FOUND});
           }
       }).catch((err)=>{
           console.log("User list err: ", err);
           return next(setError(MSG.INTERNAL_ERROR, 500));
       });
   }; 

   const UpdateRegisteredUser = (req, res, next) => {
    const { id } = req.params;
  
  
    const UpdateInfo = req.body;
    UpdateInfo.updated_at = new Date();
  
    Models.User.findOneAndUpdate(
      { _id: ObjectId(id) },
      {
        $set: UpdateInfo,
      },
      { new: true },
      (err, user) => {
        if (err) {
          console.log("Update user err: ", err);
          return next(setError(MSG.INTERNAL_ERROR, 500));
        } else if (!user) {
          return next(setError(MSG.DATA_NOT_FOUND));
        } else {
          return res.send({ data: user, message: MSG.SUCCESS });
        }
      }
    );
  };

  const DeleteUser = (req, res, next) => {
    const { id } = req.params;
  
  
    const UpdateInfo = req.body;
    UpdateInfo.updated_at = new Date();
  
    Models.User.findOneAndUpdate(
      { _id: ObjectId(id) },
      {
        $set: UpdateInfo,
      },
      { new: true },
      (err, user) => {
        if (err) {
          console.log("Update user err: ", err);
          return next(setError(MSG.INTERNAL_ERROR, 500));
        } else if (!user) {
          return next(setError(MSG.DATA_NOT_FOUND));
        } else {
          return res.send({ data: user, message: MSG.SUCCESS });
        }
      }
    );
  };

  

  module.exports = {
    UserRegister,
    GetAllUsers,
    UpdateRegisteredUser,
    DeleteUser
  };
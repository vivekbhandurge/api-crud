module.exports = {
    validate: function (rules) {
        return function (req, res, next) {
            var validation = new Validator(req.body, rules);
            if (validation.fails()) {
                if (!_.isUndefined(req.files) && !_.isEmpty(req.files)) {
                    _.each(req.files, function (file) {
                        Func.deleteFile(file.path);
                    });
                }
                var error = validation.errors.all();
                return next(setError(error[Object.keys(error)[0]][0]));
            } else {
                return next();
            }
        };
    }
}